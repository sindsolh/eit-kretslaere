﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronStraight : MonoBehaviour
{
    // Hardcoded based on the start and end local position of the electron
    public const float STARTPOS_Z = 0.0f;
    public const float ENDPOS_Z = 0.5f;
    public bool powerOn = true;

    // Start is called before the first frame update. 
    void Start()
    {
        // EMPTY
    }

    // Update is called once per frame
    void Update()
    {
        // Update positon
        this.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + Time.deltaTime / 10);

        // Reset position if at the end of the wire
        if (transform.localPosition.z > ENDPOS_Z)
        {
            this.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, STARTPOS_Z);
        }
    }
}
