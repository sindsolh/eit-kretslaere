﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronBendFlip : MonoBehaviour
{
    // Hardcoded based on the start and end local position of the electron
    public const float STARTPOS_X = -0.5f;
    public const float STARTPOS_Z = 0.5f;
    public const float CORNERPOS_X = 0.0f;
    public const float ENDPOS_Z = 0.0f;

    public int speed_x = 0;
    public int speed_z = 0;

    public bool powerOn = true;

    // Start is called before the first frame update. 
    void Start()
    {
        //this.transform.localPosition = new Vector3(STARTPOS_X, transform.localPosition.y, transform.localPosition.z);
        speed_x = 1;
        speed_z = 0;

}

    // Update is called once per frame
    void Update()
    {
        // Update positon
        this.transform.localPosition = new Vector3(transform.localPosition.x + Time.deltaTime / 10 * speed_x , transform.localPosition.y , transform.localPosition.z - Time.deltaTime / 10 * speed_z);

        // First part of loop
        if(speed_x > 0)
        {
            // Reached corner
            if (transform.localPosition.x > CORNERPOS_X) 
            {
                speed_x = 0;
                speed_z = 1;
            }
        }
        // Second part of loop
        else
        {
            // Reached the end of the wire
            if (transform.localPosition.z < ENDPOS_Z)
            {
                this.transform.localPosition = new Vector3(STARTPOS_X, transform.localPosition.y, STARTPOS_Z);
                speed_x = 1;
                speed_z = 0;
            }

        }


        if (Input.GetMouseButtonDown(0))
        {
            if (powerOn)
            {
                GetComponent<MeshRenderer>().enabled = false;
            }
            else
            {
                GetComponent<MeshRenderer>().enabled = true;
            }

            powerOn = !powerOn;
        }

         /* FOR MOBILE */
           /*
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            // Toggle off when on and vice verca
            if (touch.phase == TouchPhase.Began)
            {
                if (powerOn)
                {
                    GetComponent<MeshRenderer>().enabled = false;
                }
                else
                {
                    GetComponent<MeshRenderer>().enabled = true;
                }

                powerOn = !powerOn;
            }
        }
           */
    }
}