﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBulb : MonoBehaviour
{
    public bool lightOn;
    public Material mat;

    // Start is called before the first frame update
    void Start()
    {
        //Fetch the Material from the Renderer of the GameObject
        mat = GetComponent<MeshRenderer>().material;
        lightOn = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            if (lightOn)
            {
                mat.DisableKeyword("_EMISSION");
            }
            else
            {
                mat.EnableKeyword("_EMISSION");

            }
            lightOn = !lightOn;
        }

        /* FOR MOBILE */
        /*
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                if (lightOn)
                {
                    mat.DisableKeyword("_EMISSION");
                }
                else
                {
                    mat.EnableKeyword("_EMISSION");

                }
                lightOn = !lightOn;
            }
        }
        */
    }
}

