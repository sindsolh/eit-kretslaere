﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    public int flip;

    // Start is called before the first frame update
    void Start()
    {
        flip = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            this.transform.Rotate(0.0f, 0.0f, 90.0f * flip, Space.Self);
            flip = flip * -1;
        }

        /* FOR MOBILE */
        /*
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if(touch.phase == TouchPhase.Began)
            {
                this.transform.Rotate(0.0f, 0.0f, 90.0f * flip, Space.Self);
                flip = flip * -1;
            }
        }
        */
    }
}
